var DDB = (function(ApiBaseUrl, API_KEY, ImgSrcPrefix){
  var ddb = {};

  ddb.ApiBaseUrl = ApiBaseUrl;
  ddb.API_KEY = API_KEY;
  ddb.ImgSrcPrefix = ImgSrcPrefix;

  ddb.ApiInstitutions = "/institutions";
  ddb.ApiSearch = "/search";
  ddb.ApiGet = "/items";
  ddb.ApiParamSector = "sector";
  ddb.ApiParamQuery = "query";
  ddb.ApiParamInstitutionName = "facet=provider_fct&provider_fct";
  ddb.ApiParamStateName = "facet=state&state";
  ddb.ApiParamTitle = "facet=title&title";
  ddb.ApiHeaderAuthName = "Authorization";
  ddb.ApiHeaderAuthValue = "OAuth oauth_consumer_key=\"" + API_KEY + "\"";

  ddb.states = [
    "Baden-Württemberg",
    "Bayern",
    "Berlin",
    "Brandenburg",
    "Bremen",
    "Hamburg",
    "Hessen",
    "Mecklenburg-Vorpommern",
    "Niedersachsen",
    "Nordrhein-Westfalen",
    "Rheinland-Pfalz",
    "Saarland",
    "Sachsen",
    "Sachsen-Anhalt",
    "Schleswig-Holstein",
    "Thüringen"
  ];

  ddb.getInstitution = function(id, success){
    $.ajax({
      url: ddb.ApiBaseUrl + "/items/" + id + "/view",
      headers: { Authorization : ddb.ApiHeaderAuthValue},
      type: "GET",
      dataType: "json",
      success: success,
      error: function(jqXHR, status, msg){
        var info = "Request to DDB " + "/items" + " service for getInstitution() failed!"
        console.log(info + "[" + status + ": " + msg + "]");
        alert(info);
      }
    })
  }

  ddb.appendInstitutionsByState = function(state, domID){
    ddb.getInstitutionsByState(state, function(data){
          //("Ergebnisse: " + data.numberOfResults);
          $.each(data.results[0].docs, function(i, v){
            $("#" + domID).append("<li id=\"" + v.id + "\">" + 
              //"<a href=\"http://www.deutsche-digitale-bibliothek.de/about-us/institutions/item/" + v.id + "\">" +
              "<a href=\"#institute\" onClick=\" app.institute = '" + v.id + "';\">" +
              v.title + " (" + v.subtitle + ") " +
              "<span id=orgCount" + v.id + " class=\"ui-li-count\"></span>" +
              //"<ul id=orgObjects" + v.id + " data-role=\"listview\"></ul>" +
              "</a></li>");

            ddb.appendInstitutionsObjectCount(v.title, "orgCount" + v.id, v.id);
            //appendInstitutionObjects(v.title, 0, 5, "orgObjects" + v.id);
          });

          $("#" + domID).listview('refresh');
        });
  }

  ddb.getInstitutionsByState = function(state, success){
        //$.get(ApiBaseUrl + ApiSearch + "?" + ApiParamStateName + "=" + state + "&oauth_consumer_key=" + API_KEY, success, "json").fail( function(){
    //  alert("Request to DDB " + ApiSearch + " service for getInstitutionsByState() failed!");
    //}).setRequestHeader(ApiHeaderAuthName, ApiHeaderAuthValue);

    $.ajax({
      url: ddb.ApiBaseUrl + ddb.ApiSearch + "?" + ddb.ApiParamStateName + "=" + state + "&sort=ALPHA_ASC",
      headers: { Authorization : ddb.ApiHeaderAuthValue},
      type: "GET",
      dataType: "json",
      success: success,
      error: function(jqXHR, status, msg){
        var info = "Request to DDB " + ddb.ApiSearch + " service for getInstitutionsByState() failed!"
        console.log(info + "[" + status + ": " + msg + "]");
        alert(info);
      }
    })
  }

  ddb.appendInstitutionsObjectCount = function(institut, domID, domContainer){
    ddb.getInstitutionsObjectCount(institut, function(data){
      $("#" + domID).text(data.numberOfResults);
      if(domContainer && data.numberOfResults === 0){
        $("#" + domContainer).addClass("has-no-objects");
      }
    });
  }

  ddb.getInstitutionsObjectCount = function(institut, success){
    $.ajax({
      url: ddb.ApiBaseUrl + ddb.ApiSearch + "?" + ddb.ApiParamInstitutionName + "=" + institut + "&rows=0",
      headers: { Authorization : ddb.ApiHeaderAuthValue},
      type: "GET",
      dataType: "json",
      success: success,
      error: function(jqXHR, status, msg){
        var info = "Request to DDB " + ApiSearch + " service for getInstitutionsObjectCount() failed!"
        console.log(info + "[" + status + ": " + msg + "]");
        alert(info);
      }
    })
  }

  ddb.appendInstitutionObjects = function(institut, offset, limit, domID, query, clear){
    ddb.getInstitutionObjects(institut, offset, limit, query, function(data){
      if (clear) {
        $("#" + domID + " li").remove();
      }
      $.each(data.results[0].docs, function(i, v){
        var thumbSrc = "";
        if(v.thumbnail){
          thumbSrc = ddb.ImgSrcPrefix + v.thumbnail;
        }
        $("#" + domID).append('<li><!-- a href="#" --><img src="' + thumbSrc + '"><h2>' + v.title + '</h2><p>' + v.subtitle + '</p><!-- /a --></li>');
      });
      $("#" + domID).listview('refresh');
    });
  }

  ddb.getInstitutionObjects = function(institut, offset, limit, query, success){
    var queryDev = "";
    if (query) {
      queryDev = "&" + ddb.ApiParamTitle + "="+query;
    };
    $.ajax({
      url: ddb.ApiBaseUrl + ddb.ApiSearch + "?" + ddb.ApiParamInstitutionName + "=" + institut + queryDev +"&rows="+limit+"&offset="+offset+"&sort=ALPHA_ASC",
      headers: { Authorization : ddb.ApiHeaderAuthValue},
      type: "GET",
      dataType: "json",
      success: success,
      error: function(jqXHR, status, msg){
        var info = "Request to DDB " + ddb.ApiSearch + " service for getInstitutionObjects() failed!"
        console.log(info + "[" + status + ": " + msg + "]");
        alert(info);
      }
    })
  }

  return ddb;
})(ApiBaseUrl, API_KEY, ImgSrcPrefix);
/*


/search?query=*&rows=0&facet=affiliate_fct&affiliate_fct=Schiller%2C+Friedrich&facet=affiliate_fct&affiliate_fct=Lessing%2C+Gotthold+Ephraim&facet=affiliate_fct&affiliate_fct=Dehmel%2C+Richard&facet=affiliate_fct_role&affiliate_fct_role=Schiller%2C+Friedrich_1_affiliate_fct_involved


*/