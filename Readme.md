=== Usage ===

Rename ddb-config.js.example into ddb-config.js and edit: set your correct values for API_KEY and ApiBaseUrl.

If you want to use this app, open index.html in a browser. E.g. http://localhost/ddb-object-image/index.html.

If you want to use the *Javascript API*
* include ddb-config.js
* include ddb.js
* E.g. call DDB.getInstitute(id, function(data){...}) with the DDB-ID of an Institution and a success callback.

The Javascript API provides some *get methods* for accessing specific resources in the DDB API. Those methods primarily make the appropriate HTTP request and give back a Javascript object created from returned JSON. Usually for a get method there is a *append method* that takes the same selector (ID, query string, etc.) but a DOM node as second argument. Those methods append the returned data to an element in DOM.


=== Cross Site ===

Das öffentliche API der DDB unterstützt z.Z. noch keine Cross-Site Requests (CORS). Dieses Problem lässt sich für Javascript Andwendungen umgehen, wenn man über entsprechende Rechte an einem Apache Webserver verfügt, der unter der gleichen Domain wie die Anwendung (zur Entwickung, lokal) verfügbar ist.

Das Proxy Modul (mod_proxy) und die Erweiterung für HTTP (mod_proxy_http) müssen geladen werden. Das Vorgehen ist je nach Distribution unterschiedlich.


Die folgenden beiden Einträge sorgen dann dafür, dass Anfragen an den Apache Webserver mit dem Pfad /ddbapi auf die Domain der DDB API umgeleitet werden und Antworten so angepasst werden, dass sie aussehen als würden sie vom Webserver direkt kommen.


ProxyPass /ddbapi http://api.deutsche-digitale-bibliothek.de

ProxyPassReverse /ddbapi http://api.deutsche-digitale-bibliothek.de


Damit wird bei eine Anfrage an z.B. 

http://localhost/ddbapi/search?query=Loreley

der Request vom Webserver (!) an 

http://api.deutsche-digitale-bibliothek.de/search?query=Loreley

weitergeleitet, die Antwort wird vom Webserver entgegen genommen und an den Aufrufer zurück gegeben.


Eine Javascript Anwendung muss so keinen Cross-Site Request ausführen.


Siehe auch http://httpd.apache.org/docs/2.2/mod/mod_proxy.html#proxypass.


== DDB API ==

Just some facts I needed for the implementation.


=== Facets ===


==== All Facets ====

/search/facets


==== Facet Values ====

For facets of type SEARCH values included in the search result are shown by default. 
For other facets the facet name should be given by the facet parameter (for no results see below).

/search?query=&facet=category
{{{
 "facets":[
   {
     "numberOfFacets":0,
     "facetValues":[],
     "field":"affiliate_fct"
   },
   {
     "numberOfFacets":2,
     "facetValues":[
       {"count":477766,"value":"Kultur"},
       {"count":2065,"value":"Institution"}
     ],
     "field":"category"
   },
   {
     "numberOfFacets":0,
     "facetValues":[],
     "field":"type_fct"
   },
   {
     "numberOfFacets":0,"facetValues":[],"field":"place_fct"
   },
   {"numberOfFacets":0,"facetValues":[],"field":"time_fct"
   },
   {"numberOfFacets":0,"facetValues":[],"field":"language_fct"},
   {"numberOfFacets":0,"facetValues":[],"field":"provider_fct"},
   {"numberOfFacets":0,"facetValues":[],"field":"sector_fct"},
   {"numberOfFacets":0,"facetValues":[],"field":"keywords_fct"}
 ]
}}}

In order to just get the values for a specific facet but no search results add parameter rows set to 0.

/search?query=&facet=category&rows=0

In both cases the values are related to an added search or facet selection. E.g.

/search?query=&facet=state&state=Hessen&facet=category&rows=0

gives only values that appear in hits for state=Hessen. And 

/search?query=Hessen&facet=category&rows=0

gives only values that appear in hits for fulltext search for "Hessen".


=== Institutions ===


==== Institutions for a state ====

/search?facet=state&state=Hessen


==== Institutions from a sector ====

Should work for 'subsector' in the same way.

/search?facet=sector&sector=sec_07


==== Institutions per first character ====

Tested with api-t1, because of a bug 'query' is added.

/search?query=&facet=atoz&atoz=z
